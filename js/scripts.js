 
if (document.documentElement.clientWidth < 992) {
    $('.main-menu ul a.opener').click(function () {
      $(this).parent().find("ul:first").slideToggle();
      $(this).parent().toggleClass('active');
      return false;
    });
  
}

$(document).ready(function () {

  $('.burger').click(function () {
    if ($('.main-menu,.burger').hasClass('open')) {
      $('.main-menu,.burger').removeClass('open');
    } else {
      $('.main-menu,.burger').addClass('open');
    }
  });

  $('.tabgroup > div').hide();
  $('.tabgroup > div:first-of-type').show();
  $('.tabs a').click(function (e) {
    e.preventDefault();
    var $this = $(this),
      tabgroup = '#' + $this.parents('.tabs').data('tabgroup'),
      others = $this.closest('li').siblings().children('a'),
      target = $this.attr('href');
    others.removeClass('active');
    $this.addClass('active');
    $(tabgroup).children('div').hide();
    $(target).show();

  });

  function moveElement(selector, target) {
    var homeSelector = selector + '-home';
    var home = document.querySelector(homeSelector);
    if (home) {
      return;
    }
  
    var element = document.querySelector(selector);
    var oldParent = element.parentElement;
    var newParent = document.querySelector(target);
    if (element && newParent) {
      newParent.appendChild(element);
      oldParent.classList.add(homeSelector.slice(1));
    }
  }
  
  function moveElementBack(selector) {
    var homeSelector = selector + '-home';
    var home = document.querySelector(homeSelector);
    if (!home) {
      return;
    }
  
    var element = document.querySelector(selector);
    if (element) {
      home.appendChild(element);
      home.classList.remove(homeSelector.slice(1));
    }
  }
  
  function handleResize() {
    var width = window.innerWidth;
  
  
    if (width < 992) {
      moveElement(".main-header__right", ".main-menu");
  
      return;
    }

  
    moveElementBack(".main-header__right");
  }
  
  
  
  window.addEventListener("resize", handleResize);
  handleResize();
  

});